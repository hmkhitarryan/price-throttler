package com.price.processor;

import com.price.processor.concurrency.CcyPairProducer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

public class App {
    private static final Logger logger = Logger.getLogger(CcyPairProducer.class.getName());

    public static void main(String[] args) {
        int subscriberCount = 100;
        PriceThrottler throttler = new PriceThrottler();
        for (int i = 0; i < subscriberCount; i++) {
            PriceProcessor subscriber = new Subscriber(UUID.randomUUID());
            throttler.subscribe(subscriber);
        }

        int upstreamDataCount = 20;
        Random r = new Random();
        for (int i = 0; i < upstreamDataCount; i++) {
            throttler.onPrice("EURUSD", getRandomRoundedDouble(r));
        }

        logger.info("finished all execution");
    }

    private static double getRandomRoundedDouble(Random r) {
        double d = r.nextDouble() * 100;
        BigDecimal bd = BigDecimal.valueOf(d);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
