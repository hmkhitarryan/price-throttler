package com.price.processor.dto;

import java.util.Objects;

public class CcyPairDto {
    private final String ccyPair;
    private final double rate;

    public CcyPairDto(String ccyPair, double rate) {
        this.ccyPair = ccyPair;
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CcyPairDto pairDto = (CcyPairDto) o;
        return Double.compare(pairDto.rate, rate) == 0 && Objects.equals(ccyPair, pairDto.ccyPair);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ccyPair, rate);
    }

    @Override
    public String toString() {
        return "CcyPairDto{" +
                "ccyPair='" + ccyPair + '\'' +
                ", rate=" + rate +
                '}';
    }
}
