package com.price.processor.concurrency;

import com.price.processor.dto.CcyPairDto;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CcyPairConsumer implements Runnable {
    private static final Logger logger = Logger.getLogger(CcyPairConsumer.class.getName());

    private final BlockingQueue<CcyPairDto> queue;

    public CcyPairConsumer(BlockingQueue<CcyPairDto> theQueue) {
        this.queue = theQueue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                CcyPairDto pairDto = queue.take();
                logger.info("Consumer queue size : " + queue.size());
                take(pairDto);
            }
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, "Something went wrong when consuming the the ccyPair");
        }
    }

    void take(CcyPairDto obj) {
        logger.info("Consuming object " + obj);
    }

    @Override
    public String toString() {
        return "CcyPairConsumer{" +
                "queue=" + queue +
                '}';
    }
}
