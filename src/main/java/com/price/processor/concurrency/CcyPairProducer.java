package com.price.processor.concurrency;

import com.price.processor.dto.CcyPairDto;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CcyPairProducer implements Runnable {
    private static final Logger logger = Logger.getLogger(CcyPairProducer.class.getName());

    // a ccyPair collection holder queue for the situation,
    // when slow consumer is trying to consume, but coming data is
    // more than data that must be consumed
    private final BlockingQueue<CcyPairDto> queue;
    private final CcyPairDto ccyPairDto;

    public CcyPairProducer(BlockingQueue<CcyPairDto> queue, CcyPairDto ccyPairDto) {
        this.queue = queue;
        this.ccyPairDto = ccyPairDto;
    }

    @Override
    public void run() {
        try {
            while (true) {
                queue.put(ccyPairDto);
                logger.info("producer queue size : " + queue.size());
            }
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, "Something went wrong when consuming the the ccyPair");
        }
    }

    @Override
    public String toString() {
        return "CcyPairProducer{" +
                "queue=" + queue +
                ", ccyPairDto=" + ccyPairDto +
                '}';
    }
}
