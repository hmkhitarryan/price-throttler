package com.price.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PriceThrottler implements PriceProcessor {

    private final List<PriceProcessor> subscribers = new ArrayList<>();

    public List<PriceProcessor> getSubscribers() {
        return subscribers;
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        System.out.println("PriceThrottler onPrice");
        List<PriceProcessor> subscribers = getSubscribers();
        subscribers.forEach(s -> s.onPrice(ccyPair, rate));
    }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        synchronized (this) {
            subscribers.add(priceProcessor);
        }
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) {
        synchronized (this) {
            subscribers.remove(priceProcessor);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceThrottler throttler = (PriceThrottler) o;
        return Objects.equals(subscribers, throttler.subscribers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subscribers);
    }

    @Override
    public String toString() {
        return "PriceThrottler{" +
                "subscribers=" + subscribers +
                '}';
    }
}
