package com.price.processor;

import com.price.processor.concurrency.CcyPairConsumer;
import com.price.processor.concurrency.CcyPairProducer;
import com.price.processor.dto.CcyPairDto;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Subscriber implements PriceProcessor {
    // a uuid field just for identification purposes.
    private final UUID uuid;

    // a ccyPair collection holder queue for the situation,
    // when slow consumer is trying to consume, but coming data is
    // more than data that must be consumed
    private final BlockingQueue<CcyPairDto> sharedQueue;

    public Subscriber(UUID uuid) {
        sharedQueue = new LinkedBlockingQueue<>();
        this.uuid = uuid;
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        CcyPairDto pairDto = new CcyPairDto(ccyPair, rate);
        new Thread(new CcyPairProducer(sharedQueue, pairDto)).start();
        new Thread(new CcyPairConsumer(sharedQueue)).start();
    }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        // violating SOLID Interface segregation principle
        throw new IllegalStateException();
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) {
        // violating SOLID Interface segregation principle
        throw new IllegalStateException();
    }
}
